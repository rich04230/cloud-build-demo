# CI/CD Pipeline for GCP Cloud Run using Gitlab CI
Code examples used in Building Java  applications using Cloud Build
https://cloud.google.com/build/docs/building/build-java

![gitlabci-cloudrun](images/gitlabci-cloudrun.png)

## GCP Prerequisites
- Create a GitLab Repo.
- Enable the Cloud Run API.
- Enable the Cloud Build API.

### Creating a Service Account for Google Cloud Build
- to assign roles to the service account:
  - Cloud Build Editor — to be able to build the image
  - Storage admin — Container registry uses Cloud Storage to store images
  - Editor

![service-account-roles](images/service-account-roles.png)

- Service account key
We need to generate a private key in the JSON format, to be able to log in our pipeline with this service account.

![service-account-key](images/service-account-key.png)

### Configuring GitLab CI 
- copy the GCP service account key of the JSON file to the Gitlab CI/CD variable (Setting > CI/CD > Variables)

![var](images/gitlab-ci-var.png)

### Configuring GitLab CI Pipeline for GCP
- create a **.gitlab-ci.yml** file

```yaml
variables:
  CLOUD_RUN_SERVICE_NAME: "hello-spring"
  GCP_REGION: "asia-east1"
  GCP_IMAGE: "asia-east1-docker.pkg.dev/$GCP_PROJECT_ID/app-repo/hello-spring"

.gcp-script-before: &gcp-script-before
  - echo $GCP_SA_KEY > gcloud-service-key.json
  - gcloud auth activate-service-account --key-file gcloud-service-key.json
  - gcloud config set project $GCP_PROJECT_ID

stages:
  - build
  - test
  - build_image
  - deploy

build image on GCP:
  stage: build_image
  image: google/cloud-sdk:latest
  environment: gcp
  before_script:
    - *gcp-script-before
  script:
    - gcloud builds submit . --config=cloudbuild.yaml

deploy to GCP:
  stage: deploy
  image: google/cloud-sdk
  environment: gcp
  when: manual
  before_script:
    - *gcp-script-before
  script:
    - gcloud run deploy $CLOUD_RUN_SERVICE_NAME --region $GCP_REGION --platform managed --image $GCP_IMAGE --revision-suffix=$CI_BUILD_ID
```

- [CI/CD Pipeline Flow](https://gitlab.com/rich04230/cloud-build-demo/-/pipelines/689358224)
![pipeline-flow](images/pipeline-flow.png)
