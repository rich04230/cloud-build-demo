# Cloud Build Introduction

## Build Configuration
- Structure of a build config file
```yaml
steps:
- name: string
  args: [string, string, ...]
  env: [string, string, ...]
  dir: string
  id: string
  waitFor: [string, string, ...]
  entrypoint: string
  secretEnv: string
  volumes: object(Volume)
  timeout: string (Duration format)
  script: string
```

1. Create the build config file. In your project root directory, create a file named **cloudbuild.yaml**

2. Add the steps field. The steps section in the build config file contains the build steps that you want Cloud Build to perform.

```yaml
- steps:
```

3. Add the first step. Under steps:, add a name field and point it to a container image to execute your task.

```yaml
steps:
- name: 'gcr.io/cloud-builders/docker' #a build step with a docker builder gcr.io/cloud-builders/docker, which is a container image running Docker.
```

4. Add step arguments. The args field of a step takes a list of arguments and passes them to the builder referenced by the name field.

```yaml
steps:
- name: 'gcr.io/cloud-builders/docker'
  args: ['build', '-t', 'us-central1-docker.pkg.dev/${PROJECT_ID}/my-docker-repo/my-image', '.']
```


5. Add more steps. You can add any number of build steps to your build config file by including additional name fields and pointing them to cloud builders.

```yaml
steps:
# docker build step to invoke the docker push command to push the image build in the previous step to Artifact Registry.
 - name: 'gcr.io/cloud-builders/docker'
   args: ['push', 'us-central1-docker.pkg.dev/${PROJECT_ID}/my-docker-repo/my-image']
 - name: 'gcr.io/google.com/cloudsdktool/cloud-sdk'
   entrypoint: 'gcloud'
   args: ['compute', 'instances', 'create-with-container', 'my-vm-name', '--container-image', 'us-central1-docker.pkg.dev/${PROJECT_ID}/my-docker-repo/my-image']
   env:
   - 'CLOUDSDK_COMPUTE_REGION=us-central1'
   - 'CLOUDSDK_COMPUTE_ZONE=us-central1-a'
```

## [Java Example](https://cloud.google.com/build/docs/building/build-java)
